﻿#include <iostream>
#include <math.h>

class Vector 
{
public:
	int a;
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		a = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
		std::cout << "\n" << x << " " << y << " " << z;
		std::cout << a;
	}
private:
	double x;
	double y;
	double z;
};

int main()
{
	Vector v, v1, v2;
	v.Show();
	v1.Show();
	v2.Show();
}